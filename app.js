const homeDisplay = document.getElementById('score__counter__home');
const guestDisplay = document.getElementById('score__counter__guest');
let homeCount = 0;
let guestCount = 0;


function homeOne() {
	homeCount += 1;
	homeDisplay.textContent = homeCount;
}

function homeTwo() {
	homeCount += 2;
	homeDisplay.textContent = homeCount;
}

function homeThree() {
	homeCount += 3;
	homeDisplay.textContent = homeCount;
}

function guestOne() {
	guestCount += 1;
	guestDisplay.textContent = guestCount;
}

function guestTwo() {
	guestCount += 2;
	guestDisplay.textContent = guestCount;
}

function guestThree() {
	guestCount += 3;
	guestDisplay.textContent = guestCount;
}
